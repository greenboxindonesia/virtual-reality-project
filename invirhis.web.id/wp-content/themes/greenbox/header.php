<?php
/**
 * Theme Header.
 *
 * See also inc/template-parts/header.php.
 *
 * @package Page Builder Framework
 */
defined( 'ABSPATH' ) || die( "Can't access directly" );
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
  function content(element,delay) {
    this.open = function(callback) {
      setTimeout(function() {
        $(element).fadeIn(100, function() {
          if(callback !== undefined) callback();
        });
      }, delay);
    };
    this.close = function(callback) {
      setTimeout(function() {
        $(element).fadeOut(100, function() {
          if(callback !== undefined) callback();
        });
      }, delay);
    };
  }
  var LOADER = new content('#content',100);
  $(window).on('load', function() {
    LOADER.close(function() 
    {});
  });
  </script>
  <style type="text/css">.introloader{position:fixed;top:0;right:0;bottom:0;left:0;background:#fff;z-index:1000}.spinner{position:absolute;top:50%;left:50%;margin:-25px 0 0 -25px;width:50px;height:50px;border:5px solid #efefef;border-radius:50%}.spinner-inner{position:absolute;top:-5px;right:-5px;bottom:-5px;left:-5px;border-radius:50%;border-width:5px;border-style:solid;border-color:#333 transparent transparent transparent;border-radius:50%;animation:infinite-spinning 1s linear infinite}@keyframes infinite-spinning{from{transform:rotate(0deg)}to{transform:rotate(360deg)}}@-webkit-keyframes infinite-spinning{from{-webkit-transform:rotate(0deg)}to{-webkit-transform:rotate(360deg)}}</style>
  <link rel="apple-touch-startup-image" href="<?php echo get_home_url(); ?>/pwa/apple-touch-icon.png" />
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_home_url(); ?>/pwa/apple-touch-icon.png" />
  <link href="<?php echo get_home_url(); ?>/pwa/splashscreens/iphone5_splash.png" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
  <link href="<?php echo get_home_url(); ?>/pwa/splashscreens/iphone6_splash.png" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
  <link href="<?php echo get_home_url(); ?>/pwa/splashscreens/iphoneplus_splash.png" media="(device-width: 621px) and (device-height: 1104px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />
  <link href="<?php echo get_home_url(); ?>/pwa/splashscreens/iphonex_splash.png" media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />
  <link href="<?php echo get_home_url(); ?>/pwa/splashscreens/iphonexr_splash.png" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
  <link href="<?php echo get_home_url(); ?>/pwa/splashscreens/iphonexsmax_splash.png" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3)" rel="apple-touch-startup-image" />
  <link href="<?php echo get_home_url(); ?>/pwa/splashscreens/ipad_splash.png" media="(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
  <link href="<?php echo get_home_url(); ?>/pwa/splashscreens/ipadpro1_splash.png" media="(device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
  <link href="<?php echo get_home_url(); ?>/pwa/splashscreens/ipadpro3_splash.png" media="(device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
  <link href="<?php echo get_home_url(); ?>/pwa/splashscreens/ipadpro2_splash.png" media="(device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image" />
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_home_url(); ?>/pwa/favicon-32x32.png" />
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_home_url(); ?>/pwa/favicon-16x16.png" />
  <link rel="icon" type="image/png" sizes="512x512" href="<?php echo get_home_url(); ?>/pwa/android-chrome-512x512_.png" />
  <link rel="mask-icon" href="<?php echo get_home_url(); ?>/pwa/safari-pinned-tab.svg" color="#5bbad5" />
  <meta name="msapplication-TileColor" content="#da532c" />
  <meta name="theme-color" content="#ffffff">
  <meta name="mobile-web-app-capable" content="yes" />
  <meta name="apple-touch-fullscreen" content="yes" />
  <meta name="apple-mobile-web-app-title" content="Expo" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="apple-mobile-web-app-status-bar-style" content="default" />
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?> <?php wpbf_body_schema_markup(); ?>>
<div id="content" class="introloader">
  <div class="spinner">
    <div class="spinner-inner"></div>
  </div>
</div>
<script type="module">
  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/9.6.0/firebase-app.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyAnx_JM9mkyHxkAva5CxYO8NsZSHear90U",
    authDomain: "alpha-store-77251.firebaseapp.com",
    projectId: "alpha-store-77251",
    storageBucket: "alpha-store-77251.appspot.com",
    messagingSenderId: "498851071144",
    appId: "1:498851071144:web:ebefd206062192b7012e9d"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
</script>
	<a class="screen-reader-text skip-link" href="#content" title="<?php echo esc_attr__( 'Skip to content', 'page-builder-framework' ); ?>"><?php _e( 'Skip to content', 'page-builder-framework' ); ?></a>
	<?php do_action( 'wp_body_open' ); ?>
	<?php do_action( 'wpbf_body_open' ); ?>
	<div id="container" class="hfeed wpbf-page">
		<?php do_action( 'wpbf_before_header' ); ?>
		<?php do_action( 'wpbf_header' ); ?>
		<?php do_action( 'wpbf_after_header' ); ?>