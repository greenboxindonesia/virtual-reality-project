<?php
/**
 * Template Name: Login v2
 */

$login  = (isset($_GET['login']) ) ? $_GET['login'] : 0;

?> 

<!doctype html>
<html lang="en">
   <head>
      <title><?php echo get_the_title(); ?></title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="https://cdn.greenbox.web.id/repo/loginform/login-form-17/css/style.css">
   </head>
   <body>
      <style>
         .title-header-form {
         font-weight: 990;
         font-size: 40px;
         text-transform: uppercase;
         line-height: 0.7em;
         }
         .tagline-site {
         font-size:22px;
         }
         .logo-image-login img {
            width: 45vh;
         }
      </style>
      <section class="ftco-section">
         <div class="container">
            <div class="row justify-content-center">
               <div class="col-md-6 text-center mb-5">
                  <div class="logo-image-login">
                     <a href="<?php echo home_url(); ?>"><img src="<?php echo esc_url( wp_get_attachment_url( get_theme_mod( 'custom_logo' ) ) ); ?>" alt="IMG"></a>
                     <!--
                     <h2 class="title-header-form"><a href="<?php echo home_url(); ?>"><?php echo get_bloginfo(); ?></a></h2>
                     <span class="tagline-site">( <?php echo get_bloginfo('description'); ?> )</span>
                     -->
                  </div>
               </div>
            </div>
            <div class="row justify-content-center">
               <div class="col-md-12 col-lg-10">
                  <div class="wrap d-md-flex">
                     <div class="text-wrap p-4 p-lg-5 text-center d-flex align-items-center order-md-last">
                        <div class="text w-100">
                           <h2>SELAMAT DATANG</h2>
                           <p>Silahkan gunakan form disamping untuk masuk kedalam website <?php echo get_bloginfo(); ?> kami.</p>
                           <a href="#" class="btn btn-white btn-outline-white">Mendaftar</a>
                        </div>
                     </div>
                     <div class="login-wrap p-4 p-lg-5">
                        <div class="d-flex">
                           <div class="w-100">
                              <h3 class="mb-4">LOGIN</h3>
                           </div>
                           <div class="w-100">
                              <p class="social-media d-flex justify-content-end">
                                 <a target="_blank" href="https://fb.com/greenboxindonesia" class="social-icon d-flex align-items-center justify-content-center"><span class="fa fa-facebook"></span></a>
                                 <a target="_blank" href="https://twitter.com/greenbox_id" class="social-icon d-flex align-items-center justify-content-center"><span class="fa fa-twitter"></span></a>
								 <a target="_blank" href="https://instagram.com/greenbox.id" class="social-icon d-flex align-items-center justify-content-center"><span class="fa fa-instagram"></span></a>
                              </p>
                           </div>
                        </div>
                        <form name="custom-login-form" action="<?= esc_url( site_url( 'wp-login.php', 'login_post' ) ); ?>" method="post" class="signin-form">
						<?php
							if ( $login === "failed" ) {
							echo '<div class="alert alert-danger" role="alert">' . __("<strong>ERROR:</strong> Invalid username and/or password.") . '</div>';
							} elseif ( $login === "empty" ) {
							echo '<div class="alert alert-danger" role="alert">' . __("<strong>ERROR:</strong> Username and/or Password kosong.") . '</div>';
							} elseif ( $login === "false" ) {
							echo '<div class="alert alert-success" role="alert">' . __("<strong>INFO :</strong> Anda telah keluar.") . '</div>';
							}
							?>
                           <div class="form-group mb-3" data-validate="Valid email is required: ex@abc.xyz">
                              <label class="label" for="name">Username</label>
                              <input type="text" class="form-control" name="log" id="<?= __('user_login') ; ?>"placeholder="<?= __('Username') ?>">
                           </div>
                           <div class="form-group mb-3" data-validate="Password is required">
                              <label class="label" for="password">Password</label>
                              <input class="form-control" type="password" name="pwd" id="<?= __('user_pass'); ?>" placeholder="<?= __('Password'); ?>">
                           </div>
                           <div class="form-group">
							  <input type="submit" name="wp-submit" id="<?= esc_attr(__('wp-submit')); ?>" class="form-control btn btn-primary submit px-3" value="<?= esc_attr(__('LOG IN')); ?>" />
							  <input type="hidden" name="redirect_to" value="<?= esc_url(get_home_url('/')); ?>'" />
                           </div>
                           <div class="form-group d-md-flex">
                              <div class="w-50 text-left">
                                 <label class="checkbox-wrap checkbox-primary mb-0">Ingat Saya
                                 <input type="checkbox" checked>
                                 <span class="checkmark"></span>
                                 </label>
                              </div>
                              <div class="w-50 text-md-right">
                                 <a href="#">Lupa Password</a>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <script src="https://cdn.greenbox.web.id/repo/loginform/login-form-17/js/jquery.min.js"></script>
      <script src="https://cdn.greenbox.web.id/repo/loginform/login-form-17/js/popper.js"></script>
      <script src="https://cdn.greenbox.web.id/repo/loginform/login-form-17/js/bootstrap.min.js"></script>
      <script src="https://cdn.greenbox.web.id/repo/loginform/login-form-17/js/main.js"></script>
   </body>
</html>

