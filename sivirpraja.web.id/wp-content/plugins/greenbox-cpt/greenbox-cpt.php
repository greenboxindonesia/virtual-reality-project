<?php
/*
Plugin Name: Greenbox Custom Field
Description: Greenbox Custom Field digunakan untuk penambahan jenis post baru yang di inginkan sesuai selera Anda. Edit dan tambahkan saja beberpaa fungsi diplugin ini. Oh ya plugin ini membutuhkan dukungan plugin lain seperti <strong><a href="https://wordpress.org/plugins/advanced-custom-fields/" target="_blank">Advanced Custom Field</a></strong> baik versi free maupun pro untuk pengelolaan dan penataan post baru biar lehih enak. Enjoy Bro!
Version: 1.0.0
Author: Aank Saputra
Author URI: https://aank.my.id
Plugin URI: https://aank.my.id
*/
////////////////////////////////////
//// CREATED POST TYPE REPOLINK
////////////////////////////////////
add_action( 'init', 'aank_post_urldemo' );

// The custom function to register a oret post type
function aank_post_urldemo() {
 
  // Set the labels, this variable is used in the $args array
  $labels = array(
    'name'               => __( 'URL Web' ),
    'singular_name'      => __( 'URL Web' ),
    'add_new'            => __( 'Add New URL Web' ),
    'add_new_item'       => __( 'Add New URL Web' ),
    'edit_item'          => __( 'Edit URL Web' ),
    'new_item'           => __( 'New URL Web' ),
    'all_items'          => __( 'All URL Web' ),
    'view_item'          => __( 'View URL Web' ),
    'search_items'       => __( 'Search URL Web' ),
    'featured_image'     => 'Cover URL Web',
    'set_featured_image' => 'Cover URL Web'
  );

  // The arguments for our post type, to be entered as parameter 2 of register_post_type()
  $args = array(
    'labels'            => $labels,
    'description'       => 'Kumpulan Demo Link Repoweb.',
    'public'            => true,
    'menu_position'     => 5,
    'menu_icon'         => 'dashicons-admin-links',
    'supports'          => array( 'title', 'thumbnail', 'excerpt' ), // 'editor', 'excerpt', 'comments', 'custom-fields' ),
    //'has_archive'       => true,
    'show_in_admin_bar' => true,
    'show_in_nav_menus' => true,
    'show_in_rest'      => true,
    'rewrite'           => array('slug' => '360views','with_front' => false),
    //'taxonomies'        => array('post_tag', 'urldemo'),
    'query_var'         => 'urldemo'
  );

register_post_type( 'urldemo', $args);
}

/* INACTIVE ALL SCRIPT
//add taxonomy post type
add_action( 'init', 'aank_taxonomies_urldemo', 0 );

function aank_taxonomies_urldemo() {
  $labels = array(
    'name'              => _x( 'Kliens', 'taxonomy general name' ),
    'singular_name'     => _x( 'Klien', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Kliens' ),
    'all_items'         => __( 'All Kliens' ),
    'parent_item'       => __( 'Parent Klien' ),
    'parent_item_colon' => __( 'Parent Klien:' ),
    'edit_item'         => __( 'Edit Klien' ), 
    'update_item'       => __( 'Update Klien' ),
    'add_new_item'      => __( 'Add New Klien' ),
    'new_item_name'     => __( 'New Klien' ),
    'menu_name'         => __( 'Kliens' ),
  );

  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
    //'show_tagcloud'     => true,
    'show_admin_column' => true,
    'show_in_rest' => true, // This enables the REST API endpoint
    'query_var' => true, // This allows us to append the taxonomy param to the custom post api request.
    //'rest_controller_class' => 'WP_REST_Terms_Controller',
    //'rest_base'             => 'megaweku',
    //'taxonomies' => array('megaweku', 'post_tag'),
    //'rewrite' => array('slug' => 'megaweku','with_front' => true, 'hierarchical' => false)
  );
  register_taxonomy( 'kliens', 'urldemo', $args );
}
*/
