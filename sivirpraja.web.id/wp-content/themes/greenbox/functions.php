<?php
/**
 * Functions.
 *
 * @package Page Builder Framework Child
 */

defined( 'ABSPATH' ) || die( "Can't access directly" );

/**
 * Child theme setup.
 */
function wpbf_child_theme_setup() {

	// Textdomain.
	load_child_theme_textdomain( 'page-builder-framework-child', WPBF_CHILD_THEME_DIR . '/languages' );

}
add_action( 'after_setup_theme', 'wpbf_child_theme_setup' );

/**
 * Enqueue scripts & styles.
 */
function wpbf_child_scripts() {

	// Styles.
	wp_enqueue_style( 'wpbf-style-child', WPBF_CHILD_THEME_URI . '/style.css', false, WPBF_CHILD_VERSION );

	// Scripts (uncomment if needed).
	// wp_enqueue_script( 'wpbf-site-child', WPBF_CHILD_THEME_URI . '/js/site-child.js', false, WPBF_CHILD_VERSION, true );

}
add_action( 'wp_enqueue_scripts', 'wpbf_child_scripts', 13 );

/**=========================================================
 * add custom functions code at bellow by greenboxindonesia.
 * functions for optimizing perfoment
 * =========================================================
 */
// API key loaded inside your functions ACF Pro
function my_acf_init() {    
    acf_update_setting('google_api_key', 'AIzaSyDZ3GfQeIAy22pIbQg28lMy5wl1EtXYciw');
}
add_action('acf/init', 'my_acf_init');

//Remove jQuery Migrate
function remove_jquery_migrate($scripts)
{
    if (!is_admin() && isset($scripts->registered['jquery'])) {
        $script = $scripts->registered['jquery'];
        
        if ($script->deps) { // Check whether the script has any dependencies
            $script->deps = array_diff($script->deps, array(
                'jquery-migrate'
            ));
        }
    }
}
add_action('wp_default_scripts', 'remove_jquery_migrate');

//Remove wp-emojis-release.min.js
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

//Remove wp-embed.min.js
function remove_wp_embed(){
 wp_dequeue_script( 'wp-embed' );
}
add_action( 'wp_footer', 'remove_wp_embed' );

//Remove Block Library CSS from Loading on the front-end
function remove_wp_block_library_css(){
    wp_dequeue_style( 'wp-block-library' );
    wp_dequeue_style( 'wp-block-library-theme' );
    wp_dequeue_style( 'wc-block-style' ); // Remove WooCommerce block CSS
} 
add_action( 'wp_enqueue_scripts', 'remove_wp_block_library_css', 100 );

//Remove Scripts That Won’t Work With Wp_dequeue_script
add_action('wp_print_footer_scripts', 'wra_remove_wphead_filter', 100000);
function wra_remove_wphead_filter(){
    remove_action('wp_head', 'pluginslug_add_script', 10);
}

/**=========================================================
 * Add custom functions code at bellow by greenboxindonesia.
 * Add Functions for Plugin PWA Config
 * Membutuhkan aktivasi plugin PWA Wordpress
 * https://wordpress.org/plugins/pwa/
 * Development by https://github.com/GoogleChromeLabs/pwa-wp/graphs/contributors
 * =========================================================
 */
// NOTED: SESUAIKAN DATANYA
// Manifest add GCM Send Notification
add_filter( 'web_app_manifest', function( $manifest ) {
    $manifest['gcm_sender_id'] = '498851071144';
    return $manifest;
} );
// Manifest add Contains categories to classify the app // membuat kategori untuk jenis aplikasi
add_filter( 'web_app_manifest', function( $manifest ) {
    $manifest['categories'] = ['business', 'lifestyle', 'magazines', 'news', 'productivity', 'shopping', 'books', 'utilities', 'travel'];
    return $manifest;
} );
// Manifest add Description
add_filter( 'web_app_manifest', function( $manifest ) {
    $manifest['description'] = 'Bring Store Brand to Online | Multiple Marketplace Management for store online Apps for Mobile';
    return $manifest;
} );
// Manifest add Orientation Mode
add_filter( 'web_app_manifest', function( $manifest ) {
    $manifest['orientation'] = 'portrait';
    return $manifest;
} );
// Manifest add Language
add_filter( 'web_app_manifest', function( $manifest ) {
    $manifest['lang'] = 'id';
    return $manifest;
} );
// Disable Address bar from thw PWA shortcut
add_filter( 'web_app_manifest', function( $manifest ) {
    $manifest['display'] = 'standalone';
    return $manifest;
} );
// Manifest add maskable icon
add_filter( 'web_app_manifest', function( $manifest ) {
    $manifest['icons'][] = [
        'src'     => home_url( '/pwa/android-chrome-512x512.png' ),
        'sizes'   => '512x512',
        'type'    => 'image/png',
        'purpose' => 'any maskable',
    ];
    return $manifest;
} );
// Manifest add Contains screenshots for app store listings // menampilkan screenshots keterangan aplikasi
add_filter(
    'web_app_manifest',
    static function ( $manifest ) {
        if ( ! isset( $manifest['screenshots'] ) ) {
            $manifest['screenshots'] = array();
        }
        $manifest['screenshots'] = array_merge(
            $manifest['screenshots'],
            array(
                array(
                    "label"     => "Welcome",
                    "sizes"     => "800x753",
                    "type"      => "image/png",
                    "src"       => home_url( '/pwa/screenshots/mobile/welcome-min.png' )
                ),
                array(
                    "label"     => "Home Page",
                    "sizes"     => "606x1140",
                    "type"      => "image/png",
                    "src"       => home_url( '/pwa/screenshots/mobile/screenshot-home-min.png' )
                ),
                array(
                    "label"     => "Pop Up Page",
                    "sizes"     => "606x1140",
                    "type"      => "image/png",
                    "src"       => home_url( '/pwa/screenshots/mobile/screenshot-popup-min.png' )
                ),
                array(
                    "label"     => "Etalase Page",
                    "sizes"     => "606x1140",
                    "type"      => "image/png",
                    "src"       => home_url( '/pwa/screenshots/mobile/screenshot-etalase-min.png' )
                ),
                array(
                    "label"     => "Product Page",
                    "sizes"     => "606x1140",
                    "type"      => "image/png",
                    "src"       => home_url( '/pwa/screenshots/mobile/screenshot-single-product-min.png' )
                ),
                array(
                    "label"     => "Pop Up Beli",
                    "sizes"     => "606x1140",
                    "type"      => "image/png",
                    "src"       => home_url( '/pwa/screenshots/mobile/screenshot-beli-min.png' )
                ),
                array(
                    "label"     => "Single Post",
                    "sizes"     => "606x1140",
                    "type"      => "image/png",
                    "src"       => home_url( '/pwa/screenshots/mobile/screenshot-single-post-min.png' )
                )
            )
        );
        return $manifest;
    }
);
// Add app shortcuts
add_filter(
    'web_app_manifest',
    static function ( $manifest ) {
        if ( ! isset( $manifest['shortcuts'] ) ) {
            $manifest['shortcuts'] = array();
        }
        $manifest['shortcuts'] = array_merge(
            $manifest['shortcuts'],
            array(
                array(
                    "name"        => "Beta Store",
                    "short_name"  => "Beta Store",
                    "description" => "Bring Store Brand to Online | Multiple Marketplace Management for store online",
                    "url"         => "/play-later?utm_source=homescreen",
                    "icons"       => array( array( "src" => "/pwa/android-chrome-192x192.png", "sizes" => "192x192" ) )
                ),
                array(
                    "name"        => "View Subscriptions",
                    "short_name"  => "Subscriptions",
                    "description" => "Dapatkan info terbaru dari kami.",
                    "url"         => "/subscriptions?utm_source=homescreen",
                    "icons"       => array( array( "src" => "/pwa/android-chrome-192x192.png", "sizes" => "192x192" ) )
                )
            )
        );
        return $manifest;
    }
);

/**=========================================================
 * Add custom functions code at bellow by greenboxindonesia.
 * Add Functions for private site & redirect custom login page
 * Resource https://agconsulting.altervista.org/all-you-need-to-know-about-wp-customize-login-page-without-plugin/
 * Design form by https://colorlib.com/etc/lf/Login_v1/index.html
 * Add Functions for private site
 * Resource https://www.youcanwp.com/make-wordpress-site-private/
 * =========================================================
 */
/**
* redirect all to custom login page
* Resource https://agconsulting.altervista.org/all-you-need-to-know-about-wp-customize-login-page-without-plugin/
* Design form by https://colorlib.com/etc/lf/Login_v1/index.html
*/
function redirect_login_page() {
    $login_page  = home_url( '/pawon/' );
    $page_viewed = basename($_SERVER['REQUEST_URI']);
   
    if( $page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
      wp_redirect($login_page);
      exit;
    }
  }
  add_action('init','redirect_login_page');
  
  function login_failed() {
    $login_page  = home_url( '/pawon/' );
    wp_redirect( $login_page . '?login=failed' );
    exit;
  }
  add_action( 'wp_login_failed', 'login_failed' );
  
  function verify_username_password( $user, $username, $password ) {
    $login_page  = home_url( '/pawon/' );
      if( $username == "" || $password == "" ) {
          wp_redirect( $login_page . "?login=empty" );
          exit;
      }
  }
  add_filter( 'authenticate', 'verify_username_password', 1, 3);
  
  function logout_page() {
    $login_page  = home_url( '/pawon/' );
    wp_redirect( $login_page . "?login=false" );
    exit;
  }
  add_action('wp_logout','logout_page');
  
/**
* make private wordpress must to login first
*/
/*
function wordpress_site_private(){
    global $wp;
    if (!is_user_logged_in() && $GLOBALS['pagenow'] !== 'wp-login.php'){
      wp_redirect(wp_login_url($wp -> request));
      exit;
    }
  }
add_action('wp', 'wordpress_site_private');

/**
 * Redirect to Home page if user attempts to try go to login if logged in
 * https://stackoverflow.com/questions/25918168/wordpress-redirect-when-user-is-logged-in-on-login-page
 * https://njengah.com/redirect-if-not-logged-in-wordpress/
 * https://wordpress.stackexchange.com/questions/270615/multiple-is-page-in-page-php
 */
function redirect_if_user_not_logged_in() {
    $login_page  = home_url( '/pawon/' );
	if ( is_page( array ('homepage', 'homepage-02' ) )  && ! is_user_logged_in() ) { 
        //example can be is_page ('slugname') or (155) where 155 is page ID
        //setting to multiple page
		wp_redirect($login_page);
     exit;
     // never forget this exit since its very important for the wp_redirect() to have the exit / die
   }
}
add_action( 'template_redirect', 'redirect_if_user_not_logged_in' );

// check user login for custom post type "urldemo"
// source : https://wordpress.stackexchange.com/questions/173587/how-to-redirect-all-pages-of-a-custom-post-type
function subscription_redirect_post() {
  $queried_post_type = get_query_var('urldemo');
  $login_page  = home_url( '/pawon/' );
  if ( is_single() && ! is_user_logged_in() ==  $queried_post_type ) {
    wp_redirect($login_page);
    exit;
  }
}
add_action( 'template_redirect', 'subscription_redirect_post' );

// check user login or not in page custom for login
function add_login_check()
{
    if (is_user_logged_in()) {
        if (is_page(314)){
            wp_redirect(home_url());
            exit; 
        }
    }
}
add_action('wp', 'add_login_check');
