<?php
/**
 * Template Name: Login v1
 */

$login  = (isset($_GET['login']) ) ? $_GET['login'] : 0;

?> 

<!DOCTYPE html>
<html lang="en">
   <head>
      <title><?php echo get_the_title(); ?></title>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" type="image/png" href="images/icons/favicon.ico">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw==" crossorigin="anonymous" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/hamburgers/1.1.3/hamburgers.min.css" integrity="sha512-+mlclc5Q/eHs49oIOCxnnENudJWuNqX5AogCiqRBgKnpoplPzETg2fkgBFVC6WYUVxYYljuxPNG8RE7yBy1K+g==" crossorigin="anonymous" />
      <link rel="stylesheet" type="text/css" href="https://colorlib.com/etc/lf/Login_v1/css/util.css">
      <link rel="stylesheet" type="text/css" href="https://colorlib.com/etc/lf/Login_v1/css/main.css">
   </head>
   <body>
      <div class="limiter">
         <div class="container-login100">
            <div class="wrap-login100">
               <div class="login100-pic js-tilt" data-tilt="" style="will-change: transform; transform: perspective(300px) rotateX(0deg) rotateY(0deg);">
                  <a href="<?php echo home_url(); ?>">
                     <img src="https://colorlib.com/etc/lf/Login_v1/images/img-01.png" alt="IMG">
                     <!-- <img src="<?php echo esc_url( wp_get_attachment_url( get_theme_mod( 'custom_logo' ) ) ); ?>" alt="IMG"> -->
                  </a>
               </div>
               <?php
                  //  $args = array(
                  //    'echo' => true,
                  //    'redirect' => get_home_url(),
                  //    'form_id' => 'custom-login-form',
                  //    'label_username' => __('Username or Email Address'),
                  //    'label_password' => __('Password'),
                  //    'label_remember' => __('Remember Me'),
                  //    'label_log_in' => __('Log In'),
                  //    'id_username' => __('user_login'),
                  //    'id_password' => __('user_pass'),
                  //    'id_remember' => __('rememberme'),
                  //    'id_submit' => __('wp-submit'),
                  //    'remember' => true,
                  //    'value_username' => '',
                  //    'value_remember' => false,
                  //  );
                  
                  	// wp_login_form( $args );
                  ?>
               <style>
                  .wrap-login100 {
                  width: 970px;
                  background: #fff;
                  border-radius: 10px;
                  overflow: hidden;
                  display: -webkit-box;
                  display: -webkit-flex;
                  display: -moz-box;
                  display: -ms-flexbox;
                  display: flex;
                  flex-wrap: wrap;
                  justify-content: space-between;
                  padding: 70px 95px 40px 95px;
                  }
                  .login100-form-title {
                  font-family: Poppins-Bold;
                  font-size: 22px;
                  color: #555;
                  line-height: 1.2;
                  text-align: center;
                  width: 100%;
                  display: block;
                  padding-bottom: 20px;
                  }
                  .text-center-ket {
                  padding-bottom: 15px;
                  text-align: center;
                  }
                  .p-t-136 {
                  padding-top: 60px;
                  }
                  .p-t-12 {
                  padding-top: 12px;
                  display: none;
                  }
               </style>
               <form name="custom-login-form" id="custom-login-form" action="<?= esc_url( site_url( 'wp-login.php', 'login_post' ) ); ?>" method="post">
                  <span class="login100-form-title"><?= __('SELAMAT DATANG'); ?><br><?= __('Interactive Virtual Historical Site'); ?></span>
                  <div class="text-center-ket">
                     <span class="text-center"><?= __('Silahkan Login untuk melihat website'); ?></span>
                  </div>
                  <?php
                     if ( $login === "failed" ) {
                       echo '<div class="alert alert-danger" role="alert">' . __("<strong>ERROR:</strong> Invalid username and/or password.") . '</div>';
                     } elseif ( $login === "empty" ) {
                       echo '<div class="alert alert-danger" role="alert">' . __("<strong>ERROR:</strong> Username and/or Password kosong.") . '</div>';
                     } elseif ( $login === "false" ) {
                       echo '<div class="alert alert-success" role="alert">' . __("<strong>INFO :</strong> Anda telah keluar.") . '</div>';
                     }
                     ?>
                  <div class="wrap-input100 validate-input" data-validate="Valid email is required: ex@abc.xyz">
                     <input class="input100" type="text" name="log" id="<?= __('user_login') ; ?>"placeholder="<?= __('Username') ?>">
                     <span class="focus-input100"></span>
                     <span class="symbol-input100"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                  </div>
                  <div class="wrap-input100 validate-input" data-validate="Password is required">
                     <input class="input100" type="password" name="pwd" id="<?= __('user_pass'); ?>" placeholder="<?= __('Password'); ?>">
                     <span class="focus-input100"></span>
                     <span class="symbol-input100">
                     <i class="fa fa-lock" aria-hidden="true"></i>
                     </span>
                  </div>
                  <div class="container-login100-form-btn">
                     <input type="submit" name="wp-submit" id="<?= esc_attr(__('wp-submit')); ?>" class="login100-form-btn" value="<?= esc_attr(__('Log In')); ?>" />
                     <input type="hidden" name="redirect_to" value="<?= esc_url(get_home_url('/')); ?>'" />
                  </div>
                  <div class="text-center p-t-12">
                     <span class="txt1"><?= __('Forgot'); ?></span>
                     <a class="txt2" href="#"><?= __('Username/Password?'); ?></a>
                  </div>
                  <div class="text-center p-t-136">
                     <a class="txt2" target="_blank" href="https://greenbox.web.id"><?= __('Create & Develop by Greenboxindonesia'); ?>
                     <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                     </a>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <script  src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/tilt.js/1.0.3/tilt.jquery.min.js" integrity="sha512-14AZ/DxUrlF26z6v7egDkpJHKyJRn/7ue2BgpWZ/fmqrqVzf4PrQnToy99sHmKwzKev/VZ1tjPxusuTV/n8CcQ==" crossorigin="anonymous"></script>
      <script>
         $('.js-tilt').tilt({
         	scale: 1.1
         })
      </script>
      <script src="https://colorlib.com/etc/lf/Login_v1/js/main.js"></script>
   </body>
</html>